#include "cl.h"
#define min(a, b) ((a < b)? a : b)
#define max(a, b) ((a > b)? a : b)

CPoly CPoly::operator+(const CPoly & p)
{
	int i;
	CPoly g(max(this->degree, p.degree), NULL);
	for (i = 0; i <= max(this->degree, p.degree); i++)
	{
		if (i <= min(this->degree, p.degree))
		{
			g.p[i] = this->p[i] + p.p[i];
		}
		else
		{
			g.p[i] = (this->degree > p.degree)? this->p[i] : p.p[i]; //суммирование
		}
	}
	return g;
}
/*
CPoly h(1,{1,2});
*/
CPoly::CPoly(int deg, const double *mas)
{
	int i = 0;
	this->degree = deg;
	this->p = new double[deg + 1];//создаем массив для коэффициентов.
	if (mas != NULL)
	{
		for (i = 0; i <= deg; i++)
		{
			p[i] = mas[i];
		}
	}
	else
	{
		for (i = 0; i <= deg; i++)
		{
			p[i] = 0;
		}
	}
}
/*
	h.degree = 1;
	h.p[0] = 1;
	h.p[1] = 2;
*/
/*
	
*/
CPoly & CPoly::operator=(const CPoly & p)
{
	int i;
	this->degree = p.degree; // приравнивает степени
	delete[] this->p;
	this->p = new double[this->degree + 1];
	for (i = 0; i <= p.degree; i++)
	{
		this->p[i] = p.p[i];//приравнивает коэффициенты
	}
	return *this;
}
/*
	CPoly h;
*/
CPoly::CPoly(void)
{
	this->degree = 0;
	this->p = new double[1];
	this->p[0] = 0;
}
/*
	h.degree = 0;
	h.p - созданный массив но незаполненный.
*/
CPoly::CPoly(const CPoly & p)
{
	int i;
	this->degree = p.degree;
	this->p = new double[p.degree + 1];
	for (i = 0; i <= p.degree; i++)
	{
		this->p[i] = p.p[i];
	}
}

CPoly::~CPoly(void)
{
	this->degree = 0;
	delete[] this->p;
}

CPoly CPoly::operator*(const CPoly & p)
{
	int i;
	int j;
	CPoly g((this->degree)*(this->degree), NULL);
	for (i = 0; i <= p.degree; i++)
	{
		for (j = 0; j <= this->degree; j++)
		{
			g.p[i + j] += p.p[i]*(this->p[j]); // (1 + 2x + x^2)(1 + x) = 1 + 2x + x^2 + x + 2x^2 + x^3
		}
	}
	return g;
}


CPoly CPoly::operator*(const double & k)
{
	int i;
	CPoly g(*this);
	for (i = 0; i <= g.degree; i++)
	{
		g.p[i] *= k;
	}
	return g;
}

/*
	p = q*h
	p = q*5
*/

CPoly CPoly::operator+(const double & k)
{
	CPoly g(*this);
	g.p[0] += k;
	return g;
}

void CPoly::print(void)
{
	int i;
	for (i = 0; i <= this->degree - 1; i++)
	{
		printf("%lf x^%d + ", this->p[i], i);
	}
	printf("%lf x^%d\n", this->p[this->degree], this->degree);
}
