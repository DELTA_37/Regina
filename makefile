all: prog

prog: main.o cl.o
	g++ main.o cl.o -g -o prog
main.o: main.cpp
	g++ main.cpp -c -g -o main.o
cl.o: cl.cpp cl.h
	g++ cl.cpp -c -g -o cl.o
clean:
	rm *.o
	rm prog
