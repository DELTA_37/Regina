#include "cl.h"

int main(void)
{
	double mas[4] = {1, 2, 1, 4};
	CPoly p(2, mas); // 1 + 2x + x^2
	CPoly q(2, mas); // 1 + 2x + x^2
	CPoly sig;
	sig = p*q; 
	sig.print();
	return 0;
} 
