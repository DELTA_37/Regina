#include "stdlib.h"
#include "stdio.h"

class CPoly
{
private:
	int degree;   // степень
	double *p;    //коэффициент
public:
	CPoly(int, const double *);// по числу и массиву создает полином.
	CPoly(const CPoly &); // CPoly h(1, {}); CPoly m(h);
	CPoly & operator=(const CPoly &); // приравнивание обьектов. a=b   a.operator=(b);
	CPoly(void);// он вызывается, когда мы не передаем конструктору параметров. степень = 0, коэффициенты равны 0
	~CPoly(void); //деструктор
	CPoly operator+(const CPoly &);
	CPoly operator+(const double &);
	CPoly operator*(const double &);
	CPoly operator*(const CPoly &);
	void print(void);
};

